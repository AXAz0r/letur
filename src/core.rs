use log::info;
use std::path::Path;

use crate::result::{LeturResult, HEAD_FIELDS};
use crate::state::LeturState;

pub struct LeturCore {
    state: LeturState,
    results: Vec<LeturResult>,
    finished: bool,
}

impl LeturCore {
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {
            state: LeturState::new()?,
            results: Vec::new(),
            finished: false,
        })
    }

    fn dump(&self) -> anyhow::Result<()> {
        info!("Dumping results...");
        let mut lines = vec![HEAD_FIELDS.to_string()];
        let data_dir_pth = Path::new("./data");
        if !data_dir_pth.exists() {
            std::fs::create_dir(data_dir_pth)?;
        }
        let file_name = format!(
            "{}_{}.csv",
            self.state
                .location
                .to_lowercase()
                .replace(' ', "_")
                .replace('+', "_"),
            self.state.category.to_lowercase().replace(' ', "_")
        );
        let full_path_str = format!("./data/{}", file_name);
        let full_path = Path::new(&full_path_str);
        for result in &self.results {
            if self
                .state
                .location
                .to_lowercase()
                .contains(&result.city.to_lowercase())
            {
                lines.push(result.as_csv());
            }
        }
        std::fs::write(full_path, lines.join("\n"))?;
        Ok(())
    }

    pub async fn run(&mut self) -> anyhow::Result<()> {
        info!("Started scraper...");
        while !self.finished {
            let body = self.state.get_page().await?;
            let root = scraper::Html::parse_document(&body);
            let right_sele = scraper::Selector::parse(".listing_right_section").unwrap();
            for element in root.select(&right_sele) {
                let result = LeturResult::new(element);
                self.results.push(result);
            }
            let page_num_sele = scraper::Selector::parse(".pageCount span").unwrap();
            let page_num = root
                .select(&page_num_sele)
                .last()
                .unwrap()
                .inner_html()
                .trim()
                .to_string()
                .parse::<u64>()?;
            info!("Finished parsing page {}/{}...", self.state.page, page_num);
            if page_num == self.state.page {
                self.finished = true;
                info!("Finished the last page, ending scraper.");
            } else {
                self.state.next();
            }
        }
        self.dump()?;
        Ok(())
    }
}

pub struct LeturState {
    pub page: u64,
    pub category: String,
    pub location: String,
}

impl LeturState {
    pub fn new() -> anyhow::Result<Self> {
        let mut args = pico_args::Arguments::from_env();
        Ok(Self {
            page: 1,
            category: args.opt_value_from_str("--category")?.unwrap(),
            location: args.opt_value_from_str("--location")?.unwrap(),
        })
    }

    pub fn next(&mut self) {
        self.page += 1;
    }

    pub fn get_uri(&self) -> String {
        format!(
            "https://www.yellowpages.ca/search/si/{}/{}/{}",
            self.page,
            self.category.replace(" ", "+"),
            self.location.replace(" ", "+")
        )
    }

    pub async fn get_page(&self) -> anyhow::Result<String> {
        let resp = reqwest::get(self.get_uri()).await?;
        let body = resp.text().await?;
        Ok(body)
    }
}

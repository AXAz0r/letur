use scraper::ElementRef;

pub const HEAD_FIELDS: &str = "Name,Address,City,Region,ZIP,Link,Site,Number";

pub struct LeturResult {
    pub name: String,
    pub address: String,
    pub city: String,
    pub region: String,
    pub zip: String,
    pub link: String,
    pub site: String,
    pub number: String,
}

impl LeturResult {
    pub fn new(ele: ElementRef) -> Self {
        // Name
        let name_sele = scraper::Selector::parse(".listing__name a").unwrap();
        // Address
        let addr_sele = scraper::Selector::parse(".listing__address--full span").unwrap();
        let mut address = String::new();
        let mut city = String::new();
        let mut region = String::new();
        let mut zip = String::new();
        for addr_ele in ele.select(&addr_sele) {
            let prop = addr_ele.value().attr("itemprop").unwrap_or("");
            let value = addr_ele.inner_html().replace(',', " ");
            match prop {
                "streetAddress" => address = value,
                "addressLocality" => city = value,
                "addressRegion" => region = value,
                "postalCode" => zip = value,
                _ => {}
            }
        }
        let name_res = ele.select(&name_sele).last().unwrap();
        let name = name_res.inner_html().replace("&amp;", "&");
        let link = format!(
            "https://www.yellowpages.ca{}",
            name_res
                .value()
                .attr("href")
                .unwrap()
                .split('?')
                .collect::<Vec<&str>>()
                .first()
                .unwrap()
        );
        let num_sele = scraper::Selector::parse(".mlr__submenu__item h4").unwrap();
        let number = if let Some(num) = ele.select(&num_sele).last() {
            num.inner_html()
        } else {
            "".to_string()
        };
        let site_sele = scraper::Selector::parse(".mlr__item--website a").unwrap();
        let site = format!(
            "https://www.yellowpages.ca{}",
            if let Some(site) = ele.select(&site_sele).last() {
                site.value().attr("href").unwrap()
            } else {
                ""
            }
        );
        Self {
            name,
            address,
            city,
            region,
            zip,
            link,
            site,
            number,
        }
    }

    pub fn as_csv(&self) -> String {
        let cells = vec![
            self.name.to_string(),
            self.address.to_string(),
            self.city.to_string(),
            self.region.to_string(),
            self.zip.to_string(),
            self.link.to_string(),
            self.site.to_string(),
            self.number.to_string(),
        ];
        return cells.join(",");
    }
}

mod core;
mod result;
mod state;

use crate::core::LeturCore;
use log::info;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    log4rs::init_file("config/log.yml", Default::default())?;
    info!("Logger started.");
    let mut core = LeturCore::new()?;
    core.run().await?;
    Ok(())
}
